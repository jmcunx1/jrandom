## jrandom - print random numbers

This will print Random Numbers from /dev/urandom

It will allow you to
* print some Very Large Random Numbers
* print many Random Numbers
* print Random Numbers between a range

This works fine under Linux, BSD(s) and AIX

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jrandom) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jrandom.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jrandom.gmi (mirror)

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
